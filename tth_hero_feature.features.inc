<?php
/**
 * @file
 * wdc_hero_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function tth_hero_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
