Required Modules for WDC Hero Feature:

Bean
https://www.drupal.org/project/bean

Color Field
https://www.drupal.org/project/color_field

CTools
https://www.drupal.org/project/ctools

Features
https://www.drupal.org/project/features

Image - Core module, enabled

Link
https://www.drupal.org/project/link

List
https://www.drupal.org/project/list

Markup
https://www.drupal.org/project/markup

Media (7.x-2.x-dev - as of 1.23.17)
https://www.drupal.org/project/media

Number - Core module, enabled

Options - Core module, enabled

SliderField
https://www.drupal.org/project/sliderfield

Text - Core module, enabled