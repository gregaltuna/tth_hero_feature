<?php
/**
 * @file
 * tth_hero_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function tth_hero_feature_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_additional_features_instru'.
  $field_bases['field_additional_features_instru'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_additional_features_instru',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 2rem 0 0 0;">
<h3 style="margin-top: 0;">Additional Features Overlay</h3>
<p style="margin-bottom: 0;">If you are building a slide/BAP for the top of the Faculty/Staff or Alumni pages, and want to provide links to additional Feature items... either internal or external... you\'ll need to upload an appropriate image and link for each item. The "Title" field for this link will ultimately print on the screen as the label. Put your shortened "headline" here.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_feature_deck'.
  $field_bases['field_feature_deck'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_feature_deck',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_feature_headline'.
  $field_bases['field_feature_headline'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_feature_headline',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_feature_image'.
  $field_bases['field_feature_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_feature_image',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_feature_links'.
  $field_bases['field_feature_links'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_feature_links',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_feature_links_instructions'.
  $field_bases['field_feature_links_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_feature_links_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 3rem 0 0 0;">
<h4 style="margin-top: 0;">Feature Links</h4>
<p style="margin-bottom: 0;">This is where you add links to your overlay. Our best advice is to restrict the number of buttons to no more than two (2). Although more will print, they will not line up very well.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_align'.
  $field_bases['field_hero_align'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_align',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'top left' => 'Left Top',
        'center left' => 'Left Center',
        'bottom left' => 'Left Bottom',
        'top center' => 'Center Top',
        'center center' => 'Center Center',
        'bottom center' => 'Center Bottom',
        'top right' => 'Right Top',
        'center right' => 'Right Center',
        'bottom right' => 'Right Bottom',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_hero_deck_html'.
  $field_bases['field_hero_deck_html'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_deck_html',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_hero_deck_instructions'.
  $field_bases['field_hero_deck_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_deck_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 2.5rem 0 .5rem 0;">
<h4 style="margin-top: 0;">Hero Deck - Plain vs. HTML</h4>
<p style="margin-bottom: 0;">Your Hero Deck can be simple text by using the <strong>Hero Deck - Plain</strong> field, or you can use the <strong>Hero Deck - HTML</strong> field if you want/need to add links or other HTML to any of the words your deck. If you print a <strong>Hero Deck - Plain</strong>, the <strong>Hero Deck - HTML</strong> will not print out on the screen. We strongly urge that you do not put use any extra HTML to layout the content in this field. Although it will print, the area is not set up to be react to different layouts.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_feature_standard'.
  $field_bases['field_hero_feature_standard'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_feature_standard',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 1rem 0 .5rem 0;">
<h3 style="margin-top: 0;">Hero Feature - Standard</h3>
<p>This page will guide you through creating a <strong>Hero Feature</strong> that you can place at the top of any of your pages. After you have created your new <strong>Hero Feature</strong>, it will appear as an available Block that you may place in the "Hero Feature" region on the <a href="/admin/structure/block">Blocks Admin UI</a> page.</p>
<p>It is important that you give your new Hero Feature a contextual "Label" that you will be able to quickly recognize on the Blocks Admin UI page. For instance, "HERO - Front Page" is ideal for a Hero Feature for the home page.</p>
<p style="margin-bottom: 0;">The "Title" field will not print inside the Hero Feature, so there is no need to give your Hero Feature a Title.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_headline_instructions'.
  $field_bases['field_hero_headline_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_headline_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 2rem 0 .5rem 0;">
<h3 style="margin-top: 0;">Hero Headline</h3>
<p style="margin-bottom: 0;">This is the big text that is at the top of your overlay content.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_image'.
  $field_bases['field_hero_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_hero_image_instructions'.
  $field_bases['field_hero_image_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 1rem 0;">
<h3 style="margin-top: 0;">Primary Hero Image</h3>
<p style="margin-bottom: 0;">This is where you upload the image for your standard Hero Feature. The <strong>.jpg</strong> file format works best. With Photoshop, or other image editing program, make sure you export the file "for the web" so that it optimizes the image as much as possible. Keep in mind that the smaller the file size is, the better your site\'s performance will be. The <strong>Primary Image will serve as the default image</strong> in the event that you do not upload multiple sizes for the different screen sizes.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_image_large'.
  $field_bases['field_hero_image_large'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_large',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_hero_image_mobile'.
  $field_bases['field_hero_image_mobile'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_mobile',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_hero_image_options'.
  $field_bases['field_hero_image_options'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_options',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 2rem 0 1rem 0;">
<h3 style="margin-top: 0;">Different Images for Different Screen Sizes</h3>
<p style="margin-bottom: 0;">While the <strong>Primary Hero Image</strong> should look great for most laptop and smaller desktop screens, it\'s not optimal to use large images for mobile devices <em>or</em> images that are too small for bigger screens. Follow the instructions for each <strong>Hero Image</strong> field below, upload size-appropriate images, and you will provide a better experience for your visitors.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_hero_image_xlarge'.
  $field_bases['field_hero_image_xlarge'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_xlarge',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_hero_image_xxlarge'.
  $field_bases['field_hero_image_xxlarge'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_image_xxlarge',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_hero_overlay_instructions'.
  $field_bases['field_hero_overlay_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hero_overlay_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 1rem 0;">
<h4 style="margin-top: 0;">Hero Overlay Content</h4>
<p style="margin-bottom: 0;">This is where you enter the content for your "Headline", your supporting "Deck" and any links that you\'d like to have in your overlay. For alignment options, please see the <strong>Overlay Options</strong> tab to the left. You will be able to add additional "inset features" via the <strong>Inset Features</strong> tab to the left.</p>
</div>

<div class="instruction-image" style="max-width: 100%;">
<img src="http://bit.ly/2m5Amx4" alt="" style="max-width: 100%;">
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_horizontal_position'.
  $field_bases['field_horizontal_position'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_horizontal_position',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'left' => 'Left',
        'centered' => 'Center',
        'right' => 'Right',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_image_align_instructions'.
  $field_bases['field_image_align_instructions'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image_align_instructions',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 1rem 0;">
<h4 style="margin-top: 0;">Background Image Alignment</h4>
<p style="margin-bottom: 0;">The best option for aligning your background image is dead center. Top/bottom center, left/right center. However, sometimes the composition of an image requires that you align the background to either the top or bottom of the container, or even to the far left or right, instead of floating it dead center.</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_inset_link'.
  $field_bases['field_inset_link'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_inset_link',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_overlay_alignment'.
  $field_bases['field_overlay_alignment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_overlay_alignment',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'left-stacked' => 'Left stacked',
        'left-copy' => 'Left content, right features',
        'center' => 'Center stacked',
        'right-stacked' => 'Right stacked',
        'right-copy' => 'Right content, left features',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_overlay_alignment_instruct'.
  $field_bases['field_overlay_alignment_instruct'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_overlay_alignment_instruct',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin: 1rem 0 .5rem 0;">
<h3 style="margin-top: 0;">Overlay Alignment</h3>
<p style="margin-bottom: 0;">If you are adding <strong>Inset Features</strong> to your overlay, and do not select a layout option, it will default to "Left content, right features". This <strong>Overlay Alignment</strong> will override any settings you apply in the <strong>Overlay Options</strong> tab on the left. </p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_overlay_instruction'.
  $field_bases['field_overlay_instruction'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_overlay_instruction',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div  class="markup-box" style="margin-top: 2rem;">
<h4 style="margin-top: 0;">Transparent Color Layer for Legibility</h4>
<p style="margin-bottom: 0;">In order to make your content more legible, you may need to put a transparent color layer between your background image and your content. Select the color and opacity of that layer, below (note: black works best).</p>
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_overlay_options_instructio'.
  $field_bases['field_overlay_options_instructio'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_overlay_options_instructio',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<div class="markup-box" style="margin-top: 1rem;">
<h4 style="margin-top: 0;">Hero Overlay Options</h4>
<p>Every content area is broken down into 12 columns. This graphic demonstrates this concept. You can set the width of your “overlay” container (here it has a black background for demonstration purposes) to any of column width. The actual width of the bounding content area is set by the theme and is not adjustable. It adjusts automatically to the screen width.</p>
<p>The settings for the overlay below are:</p>
<ul style="margin-bottom: 0;">
<li><strong>Text Alignment:</strong> Center (found in the <strong>Overlay Content</strong> tab on the left)</li>
<li><strong>Overlay Size:</strong> 8 columns</li>
<li><strong>Vertical Position:</strong> Center</li>
<li><strong>Hortizontal Position:</strong> Center</li>
</ul>
</div>

<div class="instruction-image" style="max-width: 100%;">
<img src="http://bit.ly/2mFjc5I" alt="" style="max-width: 100%;">
</div>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_overlay_size'.
  $field_bases['field_overlay_size'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_overlay_size',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'large-1' => '1 column',
        'large-2' => '2 columns',
        'large-3' => '3 columns',
        'large-4' => '4 columns',
        'large-5' => '5 columns',
        'large-6' => '6 columns',
        'large-7' => '7 columns',
        'large-8' => '8 columns',
        'large-9' => '9 columns',
        'large-10' => '10 columns',
        'large-11' => '11 columns',
        'large-12' => '12 columns',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_standard_features'.
  $field_bases['field_standard_features'] = array(
    'active' => 1,
    'cardinality' => 2,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_standard_features',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  // Exported field_base: 'field_text_alignment'.
  $field_bases['field_text_alignment'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_text_alignment',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'text-left' => 'Left',
        'text-center' => 'Center',
        'text-right' => 'Right',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_transparent_overlay'.
  $field_bases['field_transparent_overlay'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_transparent_overlay',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_transparent_overlay_color'.
  $field_bases['field_transparent_overlay_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_transparent_overlay_color',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'rgb' => array(
        0 => 'rgb',
      ),
    ),
    'locked' => 0,
    'module' => 'color_field',
    'settings' => array(
      'default_colors' => '#000000, #ffffff, #00853e, #72b844, #e5dbae, #006a31, #84b1cd, #008265, #887a68, #52301b, #c1d22d, #c0db37
        ',
    ),
    'translatable' => 0,
    'type' => 'color_field_rgb',
  );

  // Exported field_base: 'field_vertical_position'.
  $field_bases['field_vertical_position'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_vertical_position',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'over-top' => 'Top',
        'over-middle' => 'Middle',
        'over-bottom' => 'Bottom',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
