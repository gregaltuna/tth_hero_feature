<?php
/**
 * @file
 * tth_hero_feature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function tth_hero_feature_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hero_image_options|bean|hero_standard|form';
  $field_group->group_name = 'group_hero_image_options';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_hero_image';
  $field_group->data = array(
    'label' => 'Image Options',
    'weight' => '12',
    'children' => array(
      0 => 'field_hero_align',
      1 => 'field_overlay_instruction',
      2 => 'field_transparent_overlay_color',
      3 => 'field_transparent_overlay',
      4 => 'field_image_align_instructions',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hero-image-options field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_hero_image_options|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hero_images|bean|hero_standard|form';
  $field_group->group_name = 'group_hero_images';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_hero_image';
  $field_group->data = array(
    'label' => 'The Image(s)',
    'weight' => '10',
    'children' => array(
      0 => 'field_hero_image_mobile',
      1 => 'field_hero_image_large',
      2 => 'field_hero_image',
      3 => 'field_hero_image_xlarge',
      4 => 'field_hero_image_xxlarge',
      5 => 'field_hero_image_instructions',
      6 => 'field_hero_image_options',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hero-images field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_hero_images|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hero_image|bean|hero_standard|form';
  $field_group->group_name = 'group_hero_image';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hero Image',
    'weight' => '3',
    'children' => array(
      0 => 'group_hero_image_options',
      1 => 'group_hero_images',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-hero-image field-group-tabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_hero_image|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hero_overlay|bean|hero_standard|form';
  $field_group->group_name = 'group_hero_overlay';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hero Overlay',
    'weight' => '4',
    'children' => array(
      0 => 'group_overlay_content',
      1 => 'group_overlay_options',
      2 => 'group_inset_features',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-hero-overlay field-group-tabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_hero_overlay|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_inset_features|bean|hero_standard|form';
  $field_group->group_name = 'group_inset_features';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_hero_overlay';
  $field_group->data = array(
    'label' => 'Inset Features',
    'weight' => '12',
    'children' => array(
      0 => 'field_overlay_alignment',
      1 => 'field_additional_features_instru',
      2 => 'field_standard_features',
      3 => 'field_overlay_alignment_instruct',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-inset-features field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_inset_features|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overlay_content|bean|hero_standard|form';
  $field_group->group_name = 'group_overlay_content';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_hero_overlay';
  $field_group->data = array(
    'label' => 'Overlay Content',
    'weight' => '10',
    'children' => array(
      0 => 'field_hero_deck_html',
      1 => 'field_feature_links',
      2 => 'field_text_alignment',
      3 => 'field_feature_headline',
      4 => 'field_feature_deck',
      5 => 'field_hero_overlay_instructions',
      6 => 'field_hero_deck_instructions',
      7 => 'field_feature_links_instructions',
      8 => 'field_hero_headline_instructions',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-overlay-content field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_overlay_content|bean|hero_standard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overlay_options|bean|hero_standard|form';
  $field_group->group_name = 'group_overlay_options';
  $field_group->entity_type = 'bean';
  $field_group->bundle = 'hero_standard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_hero_overlay';
  $field_group->data = array(
    'label' => 'Overlay Options',
    'weight' => '11',
    'children' => array(
      0 => 'field_overlay_size',
      1 => 'field_vertical_position',
      2 => 'field_horizontal_position',
      3 => 'field_overlay_options_instructio',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-overlay-options field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_overlay_options|bean|hero_standard|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Hero Image');
  t('Hero Overlay');
  t('Image Options');
  t('Inset Features');
  t('Overlay Content');
  t('Overlay Options');
  t('The Image(s)');

  return $field_groups;
}
