<?php
/**
 * @file
 * wdc_hero_feature.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function tth_hero_feature_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'hero_standard';
  $bean_type->label = 'Hero Standard';
  $bean_type->options = '';
  $bean_type->description = 'For creating a static Hero Feature with an overlay. Has options for additional inset features.';
  $export['hero_standard'] = $bean_type;

  return $export;
}
